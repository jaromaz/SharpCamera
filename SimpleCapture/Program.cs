﻿// License:      MIT
// Author:       Benjamin N. Summerton <https://16bpp.net>
// Description:  Connect to a camera and take a photo.

using System;
using System.IO;
using System.Collections.Generic;
using SharpCamera;

namespace SimpleCapture
{
    class SimpleCapture
    {
        public static void Main(string[] args)
        {
            List<TetheredCamera> cams = TetheredCamera.Scan();

            // Got one!
            if (cams.Count > 0)
            {
                // Print info
                Console.WriteLine("Detected {0} camera(s):", cams.Count);
                foreach (TetheredCamera c in cams)
                    Console.WriteLine("  {0}", c);

                // Grab the first one and connect it, take a picture, then close
                TetheredCamera cam = cams[0];
                Console.WriteLine("Connecting to {0}", cam.Name);
                cam.Connect();
                Console.WriteLine("Taking a photo...");
                File.WriteAllBytes("capture.jpeg", cam.CaptureAsBytes());
                Console.WriteLine("Saved to \"capture.jpeg\"");
                Console.WriteLine("Closing connection to camera");
                cam.Exit();
            }
            else
                Console.WriteLine("Didn't detect any cameras");
        }
    }
}